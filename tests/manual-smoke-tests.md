# Manual smoke tests

## Activate and desactivate

1. As an administrator,
2. Enter in Admin > Plugins,
3. Activate the plugin,
4. MUST NOT create errors,
5. Deactivate the plugin,
6. There MUST NOT be errors.

## Admin controls

1. As an administrator,
2. Enter in Admin > Settings > Discussion,
3. WCPC controls MUST appear and its functions MUST work properly:
   - Privacy policy internal page
   - Privacy policy custom page
   - Privacy policy basic information
   - Privacy policy page link types
   - Privacy policy page link opening (from v.0.4.0)


## Do not allow comment

1. As an unlogged user,
2. try to create a comment without checking the WCPC box,
3. MUST NOT allow to create the comment
4. The warning page MUST appear


## Allow comment

1. As an unlogged user,
2. try to create a comment checking the WCPC box,
3. MUST allow to create the comment
4. The warning page MUST NOT appear


## The consent is saved in the database

The consent MUST be stored in the `wp_commentmeta` table, in the `meta_key` column, with the name `wpcpc_private_policy_accepted`.


## Export private data

The consent MUST be exported with the following structure:

```
{
    "name": "Consent email for personal data storage",
    "value": "user@domain.example"
},
```

## Remove private data

Consent data MUST be removed from the `wp_commentmeta` table, in the `meta_key` column, with the name `wpcpc_private_policy_accepted`.


## Compatibility with UnderStrap and/or themes that set is own fields on comment form (from v.0.4.0)
1. As an administrator,
2. Enter in Admin > Appearance > Themes,
3. Install and active Understrap theme,
4. As an unlogged user,
5. Enter in a post detail page,
6. The WCPC box MUST appears and be useful.


## Compatibility with Webmention
1. As an administrator,
2. Enter in Admin > Plugins > Add New,
3. Install and active Webmention plugin,
4. Create a comment mentioning another post URL.
5. Use Webmention plugin to generate Webmention notification in the mentioned post.
6. The Webmention notification MUST create a comment.